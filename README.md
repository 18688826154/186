# 六神TV 配置

更新时间: 2022-08-22 14:50:08

## 【推荐】 六神TV（内置多条线路，在线切换）

[下载地址](https://gitea.com/y36369999/9/raw/branch/9/1/六神TV.apk)

[使用说明](https://gitea.com/y36369999/9/raw/branch/9/README.md)

## 配置


|   名称  | 更新时间  |地址  |
|  ----  | ----  |----  |
|  iw3y.json | 2022-08-22 13:06:31 |[地址](https://box.okeybox.top/tv/iw3y.json) |
|  sx39.json | 2022-08-22 11:27:55 |[地址](https://box.okeybox.top/tv/sx39.json) |
|  91qn.json | 2022-08-22 11:27:55 |[地址](https://box.okeybox.top/tv/91qn.json) |
|  cf76.json | 2022-08-22 11:26:32 |[地址](https://box.okeybox.top/tv/cf76.json) |
|  n0zx.json | 2022-08-22 11:26:32 |[地址](https://box.okeybox.top/tv/n0zx.json) |